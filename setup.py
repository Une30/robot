from setuptools import setup, find_packages

# List of requirements
with open('requirements.txt', encoding="utf-8") as f:
    requirements = f.read().splitlines()


setup(
    name="robot-pkg",
    version="1.0.0",
    description="Robot and Table CLI app",
    packages=find_packages(),  # __init__.py folders search
    install_requires=requirements
)
