[![linting: pylint](https://img.shields.io/badge/linting-pylint-yellowgreen)](https://github.com/PyCQA/pylint)

Robot CLI App
=================
The application is a simulation of a toy robot moving on a square tabletop, of dimensions 5 units x 5 units.
- There are no other obstructions on the table surface.
- The robot is free to roam around the surface of the table, but is prevented from falling to destruction. Any movement
  that would result in the robot falling from the table is prevented, however further valid movement commands 
  are allowed.
- This application can read in commands of the following form:

```plain
PLACE X,Y,F
MOVE
LEFT
RIGHT
REPORT
```

- This program tolerates SPACE character in commands. Also it will ignore invalid commands.
- PLACE will put the toy robot on the table in position X,Y and facing NORTH, SOUTH, EAST or WEST.
- The origin (0,0) can be considered to be the SOUTH WEST most corner.
- The first valid command to the robot is a PLACE command, after that, any sequence of commands may be issued, in any order, including another PLACE command. The application should discard all commands in the sequence until a valid PLACE command has been executed.
- MOVE will move the toy robot one unit forward in the direction it is currently facing.
- LEFT and RIGHT will rotate the robot 90 degrees in the specified direction without changing the position of the robot.
- REPORT will announce the X,Y and orientation of the robot.
- A robot that is not on the table can choose to ignore the MOVE, LEFT, RIGHT and REPORT commands.

Configuration
=================
You can change table size and also enable program to print errors and print location and facing of robot after each command, by modifying the src/config.py file.

Development
=================
You can create new commands by inheriting from Command class and implementing its abstract methods.

How to Build and Run CLI
=================
First build the docker image using this command
> docker build -t registry.gitlab.com/une30/robot:latest .

also you can download the image from git registry: 
```
registry.gitlab.com/une30/robot:latest
```

Now you can run our image to create a container
> docker run -it --name my_robot_app --rm registry.gitlab.com/une30/robot:latest

Then run CLI python program by this command:
>  python src/main.py 

Now you can enter your PLACE, MOVE, RIGHT, LEFT, and REPORT commands in the container

How to Run Tests
================
Tests of this project contains unit and integration tests.
Running all tests is a stage of project pipeline.

For adding a new integration test, put your input and expected output files in tests/integration_tests/test_data.
Then build the docker image and run this command in container:
> python -m unittest




