class Report:
    def __init__(self, location, facing):
        self.x = location.x
        self.y = location.y
        self.facing = facing

    def __str__(self):
        return "{x},{y},{facing}".format(x=self.x, y=self.y, facing=self.facing.value)
