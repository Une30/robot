import shlex

from src.command_factory import CommandFactory


class CommandInterpreter:
    def __init__(self):
        self.command_factory = CommandFactory

    @staticmethod
    def command_to_array(command_line):
        """Parses string command to its params. Ignores space and Comma"""
        params_without_space = shlex.split(command_line)
        params = []
        for param in params_without_space:
            params = params + [p for p in param.split(',') if p != '']
        return params

    def execute_command(self, robot, command_line):
        command_arr = self.command_to_array(command_line=command_line)
        if len(command_arr) > 0:
            command_cls = self.command_factory.find_command(command_str=command_arr[0])
            command = command_cls(robot=robot, params=command_arr[1:])
            return command.run()
        return None
