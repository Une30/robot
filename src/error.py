class InvalidCoordinationError(Exception):
    def __init__(self, x, y):
        super().__init__()
        self.x = x
        self.y = y

    def __str__(self):
        return "Invalid Coordination x: {x_value}, y: {y_value}".format(x_value=self.x, y_value=self.y)


class RobotNotOnTableError(Exception):
    def __str__(self):
        return "Robot is not on the table"


class RobotWillFalloffTableError(Exception):
    def __str__(self):
        return "Robot will fall off the the table"


class InvalidRotationDirectionError(Exception):
    def __str__(self):
        return "Invalid rotation direction"


class InvalidCommandError(Exception):
    def __str__(self):
        return "Invalid command"
