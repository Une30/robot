from src.command_interpreter import CommandInterpreter
from src.robot import Robot
from src.table import Table
from src.config import TABLE, PRINT_ERROR, PRINT_ROBOT_LOCATION

if __name__ == '__main__':
    table = Table(max_x=TABLE.get("max_x"), max_y=TABLE.get("max_y"))
    robot = Robot(table=table)
    command_interpreter = CommandInterpreter()

    while True:
        command_line = input()
        try:
            return_value = command_interpreter.execute_command(robot=robot, command_line=command_line)
            if return_value is not None:
                print(return_value)
            if PRINT_ROBOT_LOCATION:
                print(robot.location.__str__() + " ," + robot.facing.value)
        except Exception as error:
            if PRINT_ERROR:
                print(error.__str__())
            continue
