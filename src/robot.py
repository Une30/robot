from src.enums import Direction, RotationDirection
from src.error import InvalidCoordinationError, RobotNotOnTableError, RobotWillFalloffTableError, \
    InvalidRotationDirectionError
from src.location import Location
from src.report import Report


class Robot:
    def __init__(self, table):
        self.facing = None
        self.location = None
        self.table = table

    def check_if_robot_is_on_table(self):
        if self.location is None or self.facing is None:
            raise RobotNotOnTableError()

    def check_if_robot_is_on_edge(self):
        if (self.location.x == 0 and self.facing == Direction.WEST) or \
                (self.location.y == 0 and self.facing == Direction.SOUTH) or \
                (self.location.x == self.table.max_x and self.facing == Direction.EAST) or \
                (self.location.y == self.table.max_y and self.facing == Direction.NORTH):
            raise RobotWillFalloffTableError()

    def _rotate(self, ordered_directions):
        for i, _ in enumerate(ordered_directions):
            if ordered_directions[i] == self.facing:
                self.facing = ordered_directions[i + 1]
                return

    def take_place(self, x, y, facing):
        if not (0 <= x <= self.table.max_x and 0 <= y <= self.table.max_y):
            raise InvalidCoordinationError(x=x, y=y)
        self.location = Location(x=x, y=y)
        self.facing = facing

    def move(self):
        self.check_if_robot_is_on_table()
        self.check_if_robot_is_on_edge()

        if self.facing == Direction.EAST:
            self.location.move_right()
        elif self.facing == Direction.WEST:
            self.location.move_left()
        elif self.facing == Direction.NORTH:
            self.location.move_up()
        elif self.facing == Direction.SOUTH:
            self.location.move_down()

    def rotate(self, rotation_direction):
        self.check_if_robot_is_on_table()
        if rotation_direction == RotationDirection.RIGHT:
            ordered_directions = [Direction.NORTH, Direction.EAST, Direction.SOUTH, Direction.WEST, Direction.NORTH]
        elif rotation_direction == RotationDirection.LEFT:
            ordered_directions = [Direction.NORTH, Direction.WEST, Direction.SOUTH, Direction.EAST, Direction.NORTH]
        else:
            raise InvalidRotationDirectionError
        self._rotate(ordered_directions=ordered_directions)

    def get_report(self):
        self.check_if_robot_is_on_table()
        return Report(self.location, self.facing)
