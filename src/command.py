import abc

from src.enums import RotationDirection, Direction
from src.error import InvalidCommandError


class Command:
    def __init__(self, robot, params):
        self.validate_params(params)
        self.robot = robot

    @abc.abstractmethod
    def run(self, **kwargs):
        pass

    @abc.abstractmethod
    def validate_params(self, params):
        pass


class Place(Command):
    def __init__(self, robot, params):
        super().__init__(robot=robot, params=params)

        [x, y, facing] = params
        self.x = int(x)
        self.y = int(y)
        self.facing = Direction(facing)

    def run(self, **kwargs):
        self.robot.take_place(x=self.x, y=self.y, facing=self.facing)

    def validate_params(self, params):
        if len(params) != 3:
            raise InvalidCommandError()
        [x, y, facing] = params
        if not (x.isdigit() and y.isdigit() and Direction.has_value(facing)):
            raise InvalidCommandError()


class Move(Command):
    def run(self, **kwargs):
        self.robot.move()

    def validate_params(self, params):
        if len(params) != 0:
            raise InvalidCommandError()


class RotateRight(Command):
    def run(self, **kwargs):
        self.robot.rotate(RotationDirection.RIGHT)

    def validate_params(self, params):
        if len(params) != 0:
            raise InvalidCommandError()


class RotateLeft(Command):
    def run(self, **kwargs):
        self.robot.rotate(RotationDirection.LEFT)

    def validate_params(self, params):
        if len(params) != 0:
            raise InvalidCommandError()


class Report(Command):
    def run(self, **kwargs):
        report = self.robot.get_report()
        return report.__str__()

    def validate_params(self, params):
        if len(params) != 0:
            raise InvalidCommandError()
