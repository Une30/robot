import enum


class Direction(enum.Enum):
    NORTH = "NORTH"
    SOUTH = "SOUTH"
    EAST = "EAST"
    WEST = "WEST"

    @classmethod
    def has_value(cls, value):
        return value in cls._value2member_map_


class RotationDirection(enum.Enum):
    RIGHT = "RIGHT"
    LEFT = "LEFT"


class Command(enum.Enum):
    PLACE = "PLACE"
    MOVE = "MOVE"
    ROTATE_RIGHT = "RIGHT"
    ROTATE_LEFT = "LEFT"
    REPORT = "REPORT"
