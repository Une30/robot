from src.command import Place, Move, RotateRight, RotateLeft, Report
from src.enums import Command
from src.error import InvalidCommandError


class CommandFactory:
    @staticmethod
    def find_command(command_str):
        switcher = {
            Command.PLACE.value: Place,
            Command.MOVE.value: Move,
            Command.ROTATE_RIGHT.value: RotateRight,
            Command.ROTATE_LEFT.value: RotateLeft,
            Command.REPORT.value: Report,
        }
        command = switcher.get(command_str, None)
        if command is None:
            raise InvalidCommandError
        return command
