help:
	@echo "Please use \`make <ROOT>' where <ROOT> is one of"
	@echo "  dev-dependencies           to install project developing dependencies"
	@echo "  test                       to run tests in local"
	@echo "  coverage                   to get coverage report"
	@echo "  lint                       to lint project"



dev-dependencies:
	pip install -e .

dependencies:
	pip install .

test:
	python3 -m unittest

coverage:
	py.test --cov=src tests

lint:
	find . -type f -name "*.py" | xargs pylint

