import random


def generate_test_case(commands_count):
    """gets commands count and generates commands randomly,
    after each command puts a REPORT to validate the robot has done the command correctly
    prints the commands to stdout"""
    for _ in range(commands_count):
        place_cmd = "PLACE {x},{y},{f}".format(
            x=random.choice([-2, 0, 1, 2, 3, 4, 5]),
            y=random.choice([-1, 0, 1, 2, 3, 4, 5]),
            f=random.choice(["SOUTH", "EAST", "WEST"]))
        print(random.choice([place_cmd, "MOVE", "RIGHT", "LEFT"]))
        print("REPORT")


if __name__ == '__main__':
    generate_test_case(commands_count=30)
