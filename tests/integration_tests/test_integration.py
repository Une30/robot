import unittest
import os

from src.table import Table
from src.robot import Robot
from src.command_interpreter import CommandInterpreter
from src.error import RobotNotOnTableError, RobotWillFalloffTableError, InvalidCommandError, InvalidCoordinationError


class IntegrationTestCase(unittest.TestCase):
    def test_integration(self):
        table = Table(max_x=4, max_y=4)
        command_interpreter = CommandInterpreter()
        base_directory = 'tests/integration_tests/test_data/'
        generated_output = []
        for directory in os.listdir(base_directory):
            robot = Robot(table=table)
            test_dir = base_directory + directory
            input_commands, expected_outputs = self.read_test_case(directory=test_dir)
            for cmd in input_commands:
                try:
                    output = command_interpreter.execute_command(robot=robot, command_line=cmd)
                    if output is not None:
                        generated_output.append(output)
                except (RobotWillFalloffTableError, RobotNotOnTableError, InvalidCommandError,
                        InvalidCoordinationError):
                    continue

            self.assertEqual(generated_output, expected_outputs, 'Testcase encountered error: ' + test_dir)
            generated_output = []

    @staticmethod
    def read_test_case(directory):
        with open(directory + '/input.txt', encoding="utf-8") as f1:
            input_commands = f1.read().splitlines()
        with open(directory + '/expected.txt', encoding="utf-8") as f2:
            expected_outputs = f2.read().splitlines()
        return input_commands, expected_outputs
