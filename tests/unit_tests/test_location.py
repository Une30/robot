import unittest

from src.location import Location


class LocationTestCase(unittest.TestCase):
    def test_move_up(self):
        location = Location(x=2, y=3)
        location.move_up()
        self.assertEqual(location.x, 2)
        self.assertEqual(location.y, 4)

    def test_move_down(self):
        location = Location(x=2, y=3)
        location.move_down()
        self.assertEqual(location.x, 2)
        self.assertEqual(location.y, 2)

    def test_move_right(self):
        location = Location(x=2, y=3)
        location.move_right()
        self.assertEqual(location.x, 3)
        self.assertEqual(location.y, 3)

    def test_move_left(self):
        location = Location(x=2, y=3)
        location.move_left()
        self.assertEqual(location.x, 1)
        self.assertEqual(location.y, 3)
