import unittest

from src.enums import Direction
from src.location import Location
from src.report import Report


class ReportTestCase(unittest.TestCase):

    def test_printing_report(self):
        report = Report(Location(x=1, y=2), Direction.WEST)
        self.assertEqual(report.__str__(), "1,2,WEST")
