import unittest

from src.enums import Direction, RotationDirection
from src.table import Table
from src.robot import Robot
from src.error import RobotNotOnTableError, RobotWillFalloffTableError


class RobotTestCase(unittest.TestCase):
    def test_take_place(self):
        table = Table(max_x=4, max_y=4)
        robot = Robot(table=table)
        robot.take_place(x=1, y=2, facing=Direction.WEST)
        self.assertEqual(robot.location.x, 1)
        self.assertEqual(robot.location.y, 2)
        self.assertEqual(robot.facing, Direction.WEST)

    def test_move(self):
        table = Table(max_x=4, max_y=4)
        robot = Robot(table=table)
        self.assertRaises(RobotNotOnTableError, robot.move)
        robot.take_place(x=0, y=2, facing=Direction.WEST)
        self.assertRaises(RobotWillFalloffTableError, robot.move)
        robot.take_place(x=0, y=2, facing=Direction.NORTH)
        robot.move()
        self.assertEqual(robot.location.x, 0)
        self.assertEqual(robot.location.y, 3)
        self.assertEqual(robot.facing, Direction.NORTH)

    def test_rotate_right(self):
        table = Table(max_x=4, max_y=4)
        robot = Robot(table=table)
        self.assertRaises(RobotNotOnTableError, robot.rotate, RotationDirection.RIGHT)

        robot.take_place(x=0, y=2, facing=Direction.NORTH)
        robot.rotate(RotationDirection.RIGHT)
        self.assertEqual(robot.facing, Direction.EAST)
        robot.rotate(RotationDirection.RIGHT)
        self.assertEqual(robot.facing, Direction.SOUTH)
        robot.rotate(RotationDirection.RIGHT)
        self.assertEqual(robot.facing, Direction.WEST)
        robot.rotate(RotationDirection.RIGHT)
        self.assertEqual(robot.facing, Direction.NORTH)
        self.assertEqual(robot.location.x, 0)
        self.assertEqual(robot.location.y, 2)

    def test_rotate_left(self):
        table = Table(max_x=4, max_y=4)
        robot = Robot(table=table)
        self.assertRaises(RobotNotOnTableError, robot.rotate, RotationDirection.RIGHT)

        robot.take_place(x=0, y=2, facing=Direction.NORTH)
        robot.rotate(RotationDirection.LEFT)
        self.assertEqual(robot.facing, Direction.WEST)
        robot.rotate(RotationDirection.LEFT)
        self.assertEqual(robot.facing, Direction.SOUTH)
        robot.rotate(RotationDirection.LEFT)
        self.assertEqual(robot.facing, Direction.EAST)
        robot.rotate(RotationDirection.LEFT)
        self.assertEqual(robot.facing, Direction.NORTH)
        self.assertEqual(robot.location.x, 0)
        self.assertEqual(robot.location.y, 2)

    def test_get_report(self):
        table = Table(max_x=4, max_y=4)
        robot = Robot(table=table)
        self.assertRaises(RobotNotOnTableError, robot.get_report)

        robot.take_place(x=0, y=2, facing=Direction.NORTH)
        report = robot.get_report()
        self.assertEqual(report.x, 0)
        self.assertEqual(report.y, 2)
        self.assertEqual(report.facing, Direction.NORTH)

    def test_check_if_robot_is_on_table(self):
        table = Table(max_x=4, max_y=4)
        robot = Robot(table=table)

        self.assertRaises(RobotNotOnTableError, robot.check_if_robot_is_on_table)

        robot.take_place(x=1, y=2, facing=Direction.WEST)
        robot.check_if_robot_is_on_table()

    def test_check_if_robot_is_on_edge(self):
        table = Table(max_x=4, max_y=4)
        robot = Robot(table=table)

        robot.take_place(x=3, y=0, facing=Direction.SOUTH)
        self.assertRaises(RobotWillFalloffTableError, robot.check_if_robot_is_on_edge)

        robot.take_place(x=3, y=4, facing=Direction.NORTH)
        self.assertRaises(RobotWillFalloffTableError, robot.check_if_robot_is_on_edge)

        robot.take_place(x=0, y=2, facing=Direction.WEST)
        self.assertRaises(RobotWillFalloffTableError, robot.check_if_robot_is_on_edge)

        robot.take_place(x=4, y=3, facing=Direction.EAST)
        self.assertRaises(RobotWillFalloffTableError, robot.check_if_robot_is_on_edge)
