import unittest

from src.command_factory import CommandFactory
from src.command import Place, Move, RotateRight, RotateLeft, Report


class CommandFactoryTestCase(unittest.TestCase):
    def test_find_command(self):
        command_factory = CommandFactory()
        self.assertEqual(command_factory.find_command("PLACE"), Place)
        self.assertEqual(command_factory.find_command("MOVE"), Move)
        self.assertEqual(command_factory.find_command("RIGHT"), RotateRight)
        self.assertEqual(command_factory.find_command("LEFT"), RotateLeft)
        self.assertEqual(command_factory.find_command("REPORT"), Report)
