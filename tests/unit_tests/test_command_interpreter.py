import unittest

from src.command_interpreter import CommandInterpreter


class CommandInterpreterTestCase(unittest.TestCase):

    def test_command_to_array(self):
        command_interpreter = CommandInterpreter()
        self.assertEqual(command_interpreter.command_to_array("PLACE 1,2,NORTH"), ["PLACE", "1", "2", "NORTH"])
        self.assertEqual(command_interpreter.command_to_array(
            "   PLACE   1,   2,   NORTH "), ["PLACE", "1", "2", "NORTH"]
        )
        self.assertEqual(command_interpreter.command_to_array(
            "   PLACE   1,   2,   NORTH EXTRA"),
            ["PLACE", "1", "2", "NORTH", "EXTRA"])
        self.assertEqual(command_interpreter.command_to_array("MOVE"), ["MOVE"])
